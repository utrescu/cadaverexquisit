# README #

Programa que implementa el joc surrealista de "Cadàver exquisit" (més o menys) en una web desenvolupada en Java (en concret faig servir **Spring MVC**)

![Imatge](images/screenshot.png)

> El cadàver exquisit és un joc inventat pels surrealistes consistent en la creació i invenció col·lectiva. Aquesta pràctica explora les possibilitats de la creació de textos i imatges en grup, amb aportacions parcials dels participants a l'obra final que, per als surrealistes, cercava la creació lúdica, espontània, intuïtiva, grupal i anònima alhora, accidental i tan automàtica com fos possible. El seu origen es troba a un joc de saló on els participants escrivien una part del relat i el passaven a un altre perquè aquest el continués. [(Wikipèdia)](https://ca.wikipedia.org/wiki/Cad%C3%A0ver_exquisit)

Però tot això no cal ni saber-ho perquè s'ha desenvolupat perquè faci servir [Docker](http://www.docker.io) per no haver d'instal·lar res

Els contenidors es poden gestionar amb l'eina d'orquestració [docker-compose](https://docs.docker.com/compose/install/)

## Com executar-lo? ##

Per executar l'aplicació només cal tenir **Docker** i **docker-compose** instal·lats en el sistema

Després simplement es va a l'arrel del projecte i s'executa:

    docker-compose up -d

> Pot ser que el primer cop el programa no arranqui perquè no troba la base de dades (a vegades la base de dades tarda massa en arrencar i per això el programa no la troba). Quan passa això n'hi ha prou amb repetir la comanda anterior i tot funcionarà correctament... (Ho estinc intentant solucionar però sembla que he d'esperar a la nova versió de Docker)

La primera vegada que s'executa el programa tarda una mica perquè primer s'han de descarregar els contenidors Docker i després les llibreries de Java del repositori Maven. Però les execucions posteriors són simplement uns pocs segons

Quan acabi tot el procés es podrà accedir a l'aplicació anant amb el navegador a l'adreça [http://localhost:8080](http://localhost:8080)

### Important

No garanteixo que el programa sempre funcioni ja que faig servir aquest repositori per fer proves i per tant no hi ha garanties de que la darrera versió que hi hagi funcioni.

## Què pretenc fer? ##

* Summary

No pretenc fer res elaborat sinó que simplement vull fer proves per veure les possibilitats de fer servir Docker per al desenvolupament i de pas fer servir-lo per qualsevol cosa que se m'acudeixi: fer proves a Spring MVC, proves d'acceptació amb [Robot Framework](http://robotframework.org/) ...

* Versions

No hi ha versions (tot i que si compileu el programa Maven n'hi posa ...)
