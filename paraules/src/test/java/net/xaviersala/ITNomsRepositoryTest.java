package net.xaviersala;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

import net.xaviersala.model.Nom;

/**
 * Comprova el funcionament de la base de dades fent servir DBUnit. Es
 * prova la interfície JPA de NomsRepository
 * 
 * Estrictament no crec que calgui fer-ho perquè això ja ho deuen fer
 * els desenvolupadors d'Spring però està bé saber com es fa per si
 * de cas...
 * 
 * @author xavier
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)   
// @ContextConfiguration // For Spring
@SpringApplicationConfiguration(classes = ParaulesSpringApplication.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("/noms-init.xml")
@Profile("test")
public class ITNomsRepositoryTest {
  
  @Autowired
  private NomsRepository noms;
  

  /**
   * Comprova si es pot recuperar un registre de la base de dades.
   */
  @Test
  public void testSiEsTrobaElNomPerId() {
     Nom nom = noms.findOne(1L);
     assertNotNull("El nom no hauria de ser Null", nom);     
     assertThat("No quadra el nom", nom.getSingularMasculi(), equalTo("gos"));
  }
  
  /**
   * Comprova que es poden afegir registres.
   * 
   * L'anotació permet definir com ha de quedar la base de dades amb un fitxer
   * XML
   * 
   * Per tant comprovar que hi ha tres registres no hauria de fer falta però 
   * ho faig per recordar-ho.
   */
  @Test
  @ExpectedDatabase(value="/noms-end.xml", table="noms")
  public void testAfegirUnElementFunciona() {
    
    Nom gall = new Nom("gall","gallina", "galls", "gallines");
    noms.save(gall);
    
    // Comprova que s'ha afegit un nou registre
    List<Nom> resultats = (List<Nom>) noms.findAll();
    assertThat(resultats).hasSize(3);
    
    
  }

}
