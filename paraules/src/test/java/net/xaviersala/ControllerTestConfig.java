package net.xaviersala;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test2")
public class ControllerTestConfig {

  @Bean
  ParaulesService paraulesService() {
    return Mockito.mock(ParaulesService.class);
  }

}