package net.xaviersala;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.*;
import org.springframework.web.context.WebApplicationContext;

import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;

@ActiveProfiles("test2")
//@RunWith(MockitoJUnitRunner.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ParaulesSpringApplication.class, ControllerTestConfig.class})
@WebIntegrationTest
public class ParaulesSpringApplicationTests {

  private static final String GOS = "El gos";

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;
  
  @Autowired
  ParaulesService paraulesMock; 
  
  @Before
  public void SetUp() {
    
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }
  
  @Test
  public void testParaula() throws Exception{
         
    when(paraulesMock.getParaules()).thenReturn(Arrays.asList(GOS));

     mockMvc.perform(get("/"))          
               .andExpect(status().isOk())
               .andExpect(content().contentType("text/html;charset=UTF-8"))
               .andExpect(content().string(containsString(GOS)));     
    
     verify(paraulesMock, times(1)).getParaules();
     
  }

  
	@Test
	public void contextLoads() {
	  
	  
	}

}
