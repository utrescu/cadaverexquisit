package net.xaviersala;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;

import net.xaviersala.model.FormParaula;
import net.xaviersala.model.Nom;
import net.xaviersala.model.TipusParaules;

@RunWith(SpringJUnit4ClassRunner.class)   
@SpringApplicationConfiguration(ParaulesSpringApplication.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
     TransactionalTestExecutionListener.class,
     DbUnitTestExecutionListener.class})
@WebIntegrationTest(randomPort=true)
@DatabaseSetup("/database-init.xml")
@Profile("test")
public class ITParaulesServiceTest {
  
  @Value("${local.server.port}")  
  int port;

  @Autowired
  private ParaulesService servei;

  @Test
  public void testSiSObtenenLesCincParaules() {
     
    List<String> paraules = servei.getParaules();
    assertThat(paraules).hasSize(5).doesNotContainNull();
    assertThat(paraules).isSubsetOf(Arrays.asList("El gos", "Els gossos", "La gossa", "Les gosses",
                                                              "guapo", "guapos", "guapa", "guapes", 
                                                              "estima", "estimen"));
    
//    assertThat(repository.findChecked())
//    .hasSize(2)
//    .extracting(DESCRIPTION_FIELD)
//    .containsOnly(FIRST_ITEM, THIRD_ITEM);
    
  }
  
  /**
   * Comprova que es pot afegir un nou nom a la base de dades.
   */
  @Test
  @ExpectedDatabase(value="/database-nom-add.xml")
  public void testSiEsPotAfegirUnNom() {
    
    FormParaula nom = new FormParaula(TipusParaules.NOM);
    nom.setSingularMasculi("gall");
    nom.setSingularFemeni("gallina");
    nom.setPluralMasculi("galls");
    nom.setPluralFemeni("gallines");
    servei.afegirNom(nom);        
    
  }

}
