package net.xaviersala;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import net.xaviersala.model.Adjectiu;
import net.xaviersala.model.Article;
import net.xaviersala.model.FormParaula;
import net.xaviersala.model.Genere;
import net.xaviersala.model.Nom;
import net.xaviersala.model.Verb;

/**
 * Servei de paraules.
 *
 * @author xavier
 *
 */
@Service
public class ParaulesService {

  private static final String EXCEPCIONS = "aeiouhàéè";

  private static final String LOG_RECUPERADA = "... Recuperada ";

  private static final String LOG_RECUPERARPAGINA = "... Recuperar pàgina ";

  private static final Logger LOG = LoggerFactory.getLogger(ParaulesService.class);

  private NomsRepository noms;
  private VerbsRepository verbs;
  private AdjectiuRepository adjectius;
  private ArticleRepository articles;

  private static final Random RANDOMIZER = new Random();

  /**
   * Crea el servei a partir de les fonts de dades.
   * @param articles Base de dades d'articles
   * @param noms Base de dades de noms
   * @param verbs Base de dades de verbs
   * @param adjectius Base de dades d'adjectius
   */
  @Autowired
  public ParaulesService(ArticleRepository articles, NomsRepository noms, VerbsRepository verbs, AdjectiuRepository adjectius) {
    this.articles = articles;
    this.noms = noms;
    this.verbs = verbs;
    this.adjectius = adjectius;
  }

  public List<String> getParaules() {
    List<String> paraules = new ArrayList<>();
    Genere genere = Genere.random();

    Article article = getArticle();
    if (article == null) { 
      article = new Article("El", "La", "Els","Les", true, "L'", "L'", "Els", "Les");
    }
    // Ho hauré de repetir ...
    for(int i=0; i<2; i++) {
      String nouArticle = article.getParaula(genere) + " ";
      Nom nom = getRandomNom();
      LOG.info(LOG_RECUPERADA + nom.getParaula(genere));
      if (nom != null) {
        String word = nom.getParaula(genere);

        if (article.isException()
            && EXCEPCIONS.indexOf(Character.toLowerCase(word.charAt(0))) >= 0) {
            nouArticle = article.getException(genere);
        }
        paraules.add(nouArticle + word);
      }

      Adjectiu adjectiu = getRandomAdjectiu();
      LOG.info(LOG_RECUPERADA + adjectiu.getParaula(genere));
      if (adjectiu != null) {
        paraules.add(adjectiu.getParaula(genere));
      }
    }

    Verb verb = getRandomVerb();
    LOG.info(LOG_RECUPERADA + verb.getParaula(genere));
    if (verb != null) {
      // S'ha d'afegir al mig.
      paraules.add(2, verb.getParaula(genere));
    }

    return paraules;
  }

  private Article getArticle() {
    Page<Article> pagina = articles.findAll(new PageRequest(0, 1, Direction.ASC, "id"));
    if (!pagina.hasContent()) { 
      return null; 
    } 
    return pagina.getContent().get(0);
  }
  

  private Nom getRandomNom() throws IllegalArgumentException {
    int pagina = valorAleatori((int) noms.count(), "nom");
    LOG.info(LOG_RECUPERARPAGINA + pagina);
    Page<Nom> resultat = noms.findAll(new PageRequest(pagina, 1));
    if (!resultat.hasContent()) { 
      throw new IllegalArgumentException("No hi ha cap nom"); 
    }     
    return resultat.getContent().get(0);

  }

  private Verb getRandomVerb() throws IllegalArgumentException {
    int pagina = valorAleatori((int) verbs.count(), "verb");
    Page<Verb> resultat = verbs.findAll(new PageRequest(pagina, 1));
    if (!resultat.hasContent()) { 
      throw new IllegalArgumentException("No hi ha cap verb"); 
    }     
    return resultat.getContent().get(0);

  }

  private Adjectiu getRandomAdjectiu() throws IllegalArgumentException {
    int pagina = valorAleatori((int) adjectius.count(), "adjectiu");
    Page<Adjectiu> resultat = adjectius.findAll(new PageRequest(pagina, 1));
    if (!resultat.hasContent()) { 
      throw new IllegalArgumentException("No hi ha cap adjectiu");
    }     
    return resultat.getContent().get(0);

  }

  /**
   * Serveix per triar una pàgina al atzar.
   * @return
   */
  private int valorAleatori(int total, String que) {
    
    if (total == 0) {
      return total;
    }
    return RANDOMIZER.nextInt(total);
  }

  /**
   * Afegir un nou nom a la base de dades.
   * @param paraula Paraula a afegir
   */
  public void afegirNom(FormParaula paraula) {

    Nom nom = new Nom(paraula);
    LOG.info("... desant nom " + paraula.getSingularMasculi());
    noms.save(nom);
  }

  /**
   * Afegir un nou adjectiu a la base de dades.
   * @param paraula Adjectiu a afegir
     */
  public void afegirAdjectiu(FormParaula paraula) {
    Adjectiu adjectiu = new Adjectiu(paraula);
    LOG.info("... desant adjectiu " + paraula.getSingularMasculi());
    adjectius.save(adjectiu);

  }

  /**
   * Afegir un verb a la base de dades.
   * @param paraula verb a afegir
     */
  public void afegirVeb(FormParaula paraula) {
    Verb verb = new Verb(paraula);
    LOG.info("... desant adjectiu " + paraula.getSingularMasculi());
    verbs.save(verb);

  }

}
