package net.xaviersala;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.xaviersala.config.FormParaulaValidator;
import net.xaviersala.model.FormParaula;
import net.xaviersala.model.Genere;
import net.xaviersala.model.TipusParaules;

/**
 * Controlador base del servei de paraules.
 *
 * @author xavier
 *
 */
@Controller
public class ParaulesController {

  private static final Logger LOG = LoggerFactory.getLogger(ParaulesController.class);
  private ParaulesService servei;

  /**
   * Crea el controlador a partir del servei de paraules.
   * @param servei Servei de paraules
   */
  @Autowired
  public ParaulesController(ParaulesService servei) {
    this.servei = servei;
  }

  
  /**
   * Per validar el formulari
   */
  @InitBinder("paraula")
  protected void initSubmitterBinder(WebDataBinder binder) {
      binder.setValidator(new FormParaulaValidator());
  } 
  
  /**
   * Obtenir la frase generada.
   *
   * @param model model per la plantilla
   * @return pàgina web.
   */
  @RequestMapping("/")
  public String tots(Model model) {
    LOG.info("Recuperar paraules");
    model.addAttribute("paraules", servei.getParaules());
    return "index";
  }

  /**
   * Mostra l'ajuda.
   *
   * @return pàgina d'ajuda
   */
  @RequestMapping(value="/add", method=RequestMethod.GET)
  public String ajuda() {
    return "addhelp";
  }

  /**
   * Mostra un formulari per afegir una paraula
   * d'un determinat tipus.
   *
   * @param tipus tipus de paraula
   * @param model model per la pantilla
   * @return retorna el formulari demanat
   */
  @RequestMapping("/add/{tipus}")
  public String afegirMots(@PathVariable("tipus") String tipus, Model model) {

    if (Arrays.asList(TipusParaules.getStrings()).indexOf(tipus) == -1) {
      throw new IllegalArgumentException("Tipus de paraula no existent");
    }


    model.addAttribute("paraula", new FormParaula(TipusParaules.valueOf(tipus.toUpperCase())));
    return "formulari";
  }

  /**
   * Crea la paraula en la base de dades si no existeix.
   *
   * @param paraula paraula a afegir
   * @return Torna a la pàgina principal o bé dóna un error
   */
  @RequestMapping(value="/add", method = RequestMethod.POST)
  public String alta(@ModelAttribute("paraula") @Valid FormParaula paraula, BindingResult bindingResult,
                     final RedirectAttributes redirectAttributes) {
    
    if (bindingResult.hasErrors()) {
      
      return "formulari";
    }
    String resultat = "Desat";

    switch(paraula.getTipus()) {
      case NOM:
        LOG.info("Afegint nom" + paraula.getParaula(Genere.MASCULI));
        servei.afegirNom(paraula);
        break;
      case ADJECTIU:
        LOG.info("Afegint adjectius" + paraula.getParaula(Genere.MASCULI));
        servei.afegirAdjectiu(paraula);
        break;
      case VERB:
        LOG.info("Afegint verbs" + paraula.getParaula(Genere.MASCULI));
        servei.afegirVeb(paraula);
        break;
      default:
        LOG.info("Error, el tipus no quadra");
        resultat = "Error";
        throw new IllegalArgumentException("Tipus de paraula no existent");
    }
    redirectAttributes.addFlashAttribute("paraula", paraula);
    redirectAttributes.addFlashAttribute("resultat", resultat);

    return "redirect:/";
  }

  
}
