package net.xaviersala;

import org.springframework.data.repository.PagingAndSortingRepository;

import net.xaviersala.model.Nom;

/**
 * Base de dades de noms. 
 * 
 * @author xavier
 *
 */
public interface NomsRepository extends PagingAndSortingRepository<Nom, Long> {
    
}
