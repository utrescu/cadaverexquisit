package net.xaviersala;

import org.springframework.data.repository.PagingAndSortingRepository;

import net.xaviersala.model.Adjectiu;

/**
 * Base de dades d'adjectius. 
 * 
 * @author xavier
 *
 */
public interface AdjectiuRepository extends PagingAndSortingRepository<Adjectiu, Long>{

}
