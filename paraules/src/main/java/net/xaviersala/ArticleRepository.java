package net.xaviersala;

import org.springframework.data.repository.PagingAndSortingRepository;
import net.xaviersala.model.Article;

/**
 * Recuperar els Articles de la base de dades.
 * 
 * @author xavier
 *
 */
public interface ArticleRepository extends PagingAndSortingRepository<Article, Long>{

}
