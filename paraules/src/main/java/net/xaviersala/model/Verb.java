package net.xaviersala.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Verbs a la base de dades.
 * 
 * @author xavier
 *
 */
@Entity
@Table(name = "verbs")
public class Verb extends Paraula {

  /**
   * 
   */
  private static final long serialVersionUID = 6561354689064956565L;

  public Verb() {
    // needed by JPA 
  }

  /**
   * Create an object
   * @param gos singular masculí
   * @param gosa singular femení
   * @param gossos plural masculí
   * @param gosses plural femení
   */
  public Verb(String gos, String gosa, String gossos, String gosses) {
    super(gos, gosa, gossos, gosses);
  }
  
  /**
   * Crea un verb a partir de les dades del formulari.
   * @param f
   */
  public Verb(FormParaula f) {
    super(f.getSingularMasculi(), f.getSingularFemeni(), f.getPluralMasculi(), f.getPluralFemeni());
  }
}
