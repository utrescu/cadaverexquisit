package net.xaviersala.model;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Classe base pels noms, verbs i adjectius.
 * @author xavier
 *
 */
@MappedSuperclass
public abstract class Paraula implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 295447735094331464L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  /**
   * Nom del color.
   */
  private String singularMasculi;
  private String singularFemeni;
  private String pluralMasculi;
  private String pluralFemeni;

  
  public Paraula() {    
  }
  
  /**
   * Crea l'objecte i l'inicialitza.
   * @param sm
   * @param sf
   * @param pm
   * @param pf
   */
  public Paraula(String sm, String sf, String pm, String pf) {
    singularMasculi = sm;
    singularFemeni = sf;
    pluralMasculi = pm;
    pluralFemeni = pf;
  }
  
  /**
   * @return the id
   */
  public long getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(long id) {
    this.id = id;
  }

  /**
   * @return the singularMasculi
   */
  public String getSingularMasculi() {
    return singularMasculi;
  }

  /**
   * @param singularMasculi
   *          the singularMasculi to set
   */
  public void setSingularMasculi(String singularMasculi) {
    this.singularMasculi = singularMasculi;
  }

  /**
   * @return the singularFemeni
   */
  public String getSingularFemeni() {
    return singularFemeni;
  }

  /**
   * @param singularFemeni
   *          the singularFemeni to set
   */
  public void setSingularFemeni(String singularFemeni) {
    this.singularFemeni = singularFemeni;
  }

  /**
   * @return the pluralMasculi
   */
  public String getPluralMasculi() {
    return pluralMasculi;
  }

  /**
   * @param pluralMasculi
   *          the pluralMasculi to set
   */
  public void setPluralMasculi(String pluralMasculi) {
    this.pluralMasculi = pluralMasculi;
  }

  /**
   * @return the pluralFemeni
   */
  public String getPluralFemeni() {
    return pluralFemeni;
  }

  /**
   * @param pluralFemeni
   *          the pluralFemeni to set
   */
  public void setPluralFemeni(String pluralFemeni) {
    this.pluralFemeni = pluralFemeni;
  }

  /**
   * Obtenir la paraula del gènere especificat. 
   * @param genere gènere que es vol.
   * @return paraula
   */
  public String getParaula(Genere genere) {
    String resultat;

    switch (genere) {
    case MASCULI:
      resultat = singularMasculi;
      break;
    case FEMENI:
      resultat = singularFemeni;
      break;
    case MASCULIPLURAL:
      resultat = pluralMasculi;
      break;
    case FEMENIPLURAL:
      resultat = pluralFemeni;
      break;
    default: 
      resultat = "ERROR";
    }
    return resultat;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "(" + singularMasculi + ", " + singularFemeni + ", " 
               + pluralMasculi + ", " + pluralFemeni + ")";
  }

  
  
}
