package net.xaviersala.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Gèneres de les paraules. 
 * 
 * @author xavier
 *
 */
public enum Genere {
    MASCULI, FEMENI, MASCULIPLURAL, FEMENIPLURAL;
  
  private static final List<Genere> VALUES =
      Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    /**
     * Obtenir aleatòriament un gènere. 
     * 
     * @return gènere 
     */
    public static Genere random()  {
      return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
