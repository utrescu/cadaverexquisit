package net.xaviersala.model;

/**
 * Classe per validar els formularis.
 *
 * @author xavier
 *
 */
public class FormParaula extends Paraula {

  /**
   *
   */
  private static final long serialVersionUID = -8089985547604267177L;

  private TipusParaules tipus;

  public FormParaula() {
    // Needed for the Spring Form
  }

  /**
   * Formulari de paraules.
   * @param tipus tipus de paraula a crear
   */
  public FormParaula(TipusParaules tipus) {
    this.tipus = tipus;
  }


  /**
   * @return the tipus
   */
  public TipusParaules getTipus() {
    return tipus;
  }

  /**
   * @param tipus the tipus to set
   */
  public void setTipus(TipusParaules tipus) {
    this.tipus = tipus;
  }



}
