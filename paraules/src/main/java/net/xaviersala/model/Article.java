package net.xaviersala.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Base de dades d'articles.
 *
 * @author xavier
 *
 */
@Entity
@Table(name="articles")
public class Article extends Paraula {

  /**
   *
   */
  private static final long serialVersionUID = -6137056222520704850L;

  private boolean exception;

  private String exceptionSingularMasculi;
  private String exceptionSingularFemeni;
  private String exceptionPluralMasculi;
  private String exceptionPluralFemeni;


  public Article() {
    // Needed by JPA
  }

  /**
   * Create an object
   * @param gos singular masculí
   * @param gosa singular femení
   * @param gossos plural masculí
   * @param gosses plural femení
   */
  public Article(String gos, String gosa, String gossos, String gosses, boolean exception,
      String exceptionSingularMasculi, String exceptionSingularFemeni, String exceptionPluralMasculi, String exceptionPluralFemeni) {
    super(gos, gosa, gossos, gosses);
    this.exception = exception;
    this.exceptionSingularMasculi = exceptionSingularMasculi;
    this.exceptionSingularFemeni = exceptionSingularFemeni;
    this.exceptionPluralMasculi = exceptionPluralMasculi;
    this.exceptionPluralFemeni = exceptionPluralFemeni;

  }


  /**
   * @return the exception
   */
  public boolean isException() {
    return exception;
  }

  /**
   * @param exception the exception to set
   */
  public void setException(boolean exception) {
    this.exception = exception;
  }

  /**
   * @return the exceptionSingularMasculi
   */
  public String getExceptionSingularMasculi() {
    return exceptionSingularMasculi;
  }

  /**
   * @param exceptionSingularMasculi the exceptionSingularMasculi to set
   */
  public void setExceptionSingularMasculi(String exceptionSingularMasculi) {
    this.exceptionSingularMasculi = exceptionSingularMasculi;
  }

  /**
   * @return the exceptionSingularFemeni
   */
  public String getExceptionSingularFemeni() {
    return exceptionSingularFemeni;
  }

  /**
   * @param exceptionSingularFemeni the exceptionSingularFemeni to set
   */
  public void setExceptionSingularFemeni(String exceptionSingularFemeni) {
    this.exceptionSingularFemeni = exceptionSingularFemeni;
  }

  /**
   * @return the exceptionPluralMasculi
   */
  public String getExceptionPluralMasculi() {
    return exceptionPluralMasculi;
  }

  /**
   * @param exceptionPluralMasculi the exceptionPluralMasculi to set
   */
  public void setExceptionPluralMasculi(String exceptionPluralMasculi) {
    this.exceptionPluralMasculi = exceptionPluralMasculi;
  }

  /**
   * @return the exceptionPluralFemeni
   */
  public String getExceptionPluralFemeni() {
    return exceptionPluralFemeni;
  }

  /**
   * @param exceptionPluralFemeni the exceptionPluralFemeni to set
   */
  public void setExceptionPluralFemeni(String exceptionPluralFemeni) {
    this.exceptionPluralFemeni = exceptionPluralFemeni;
  }

  /**
   * Obtenir la paraula quan es tracta d'una excepció.
   *
   * @param genere paraula.
   * @return Excepció de l'article
   */
  public String getException(Genere genere) {
      String resultat;

      switch (genere) {
      case MASCULI:
        resultat = exceptionSingularMasculi;
        break;
      case FEMENI:
        resultat = exceptionSingularFemeni;
        break;
      case MASCULIPLURAL:
        resultat = exceptionPluralMasculi;
        break;
      case FEMENIPLURAL:
        resultat = exceptionPluralFemeni;
        break;
      default:
        resultat = "ERROR";
      }
      return resultat;
    }
}
