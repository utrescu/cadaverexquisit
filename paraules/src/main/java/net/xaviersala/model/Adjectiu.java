package net.xaviersala.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Base de dades d'adjetius.
 *
 * @author xavier
 *
 */
@Entity
@Table(name="adjectius")
public class Adjectiu extends Paraula {

  /**
   *
   */
  private static final long serialVersionUID = 2539675734582757283L;

  public Adjectiu() {
    // Needed by JPA
  }

  /**
   * Create an object
   * @param gos singular masculí
   * @param gosa singular femení
   * @param gossos plural masculí
   * @param gosses plural femení
   */
  public Adjectiu(String gos, String gosa, String gossos, String gosses) {
    super(gos, gosa, gossos, gosses);
  }

  /**
   * Crea un Adjectiu a partir de les dades del formulari.
   * @param f Formulari
   */
  public Adjectiu(FormParaula f) {
    super(f.getSingularMasculi(), f.getSingularFemeni(), f.getPluralMasculi(), f.getPluralFemeni());
  }

}
