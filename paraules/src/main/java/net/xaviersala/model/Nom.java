package net.xaviersala.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Defineix els noms a la base de dades.
 * 
 * @author xavier
 *
 */
@Entity
@Table(name="noms")
public class Nom extends Paraula {
  
  /**
   * 
   */
  private static final long serialVersionUID = 2539675734582757283L;


  public Nom() {
    // needed by JPA 
  }
  
  /**
   * Create an object
   * @param gos singular masculí
   * @param gosa singular femení
   * @param gossos plural masculí
   * @param gosses plural femení
   */
  public Nom(String gos, String gosa, String gossos, String gosses) {
    super(gos, gosa, gossos, gosses);
  }
  
  /**
   * Omple amb els valors del formulari.
   * 
   * @param f
   */
  public Nom(FormParaula f) {
    super(f.getSingularMasculi(), f.getSingularFemeni(), f.getPluralMasculi(), f.getPluralFemeni());
  }
 
}
