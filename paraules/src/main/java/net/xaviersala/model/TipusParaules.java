package net.xaviersala.model;

/**
 * Tipus de paraules. 
 * 
 * @author xavier
 *
 */
public enum TipusParaules {
  
    NOM("nom"), 
    ADJECTIU("adjectiu"), 
    VERB("verb");
  
    String value;
    
    TipusParaules(String t) {
      value = t;
    }
    
    public static String[] getStrings() {
      String[] resultat = new String[values().length];
      
      int i=0;
      for(TipusParaules t: values()) {
        resultat[i] = t.value;
        i++;
      }
      return resultat;
    }
    
}
