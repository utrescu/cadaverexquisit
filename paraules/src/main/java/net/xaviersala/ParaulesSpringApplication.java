package net.xaviersala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParaulesSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParaulesSpringApplication.class, args);
	}
	
	
}
