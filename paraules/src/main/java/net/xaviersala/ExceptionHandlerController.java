package net.xaviersala;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerController {

  /**
   * Captura l'error de Bad Request.
   * 
   * @param e
   *          Excepció
   * @param response
   *          Resposta HTTP
   * @throws IOException
   *           Excepció d'entrada/Sortida
   */
  @ExceptionHandler({ IllegalArgumentException.class })
  public String ilegal(Model model, Exception ex) {
    model.addAttribute("message", ex.getMessage());
    return "400";
  }

  @ExceptionHandler(value = { Exception.class, RuntimeException.class })
  public ModelAndView defaultErrorHandler(HttpServletRequest request,
      Exception e) {

    ModelAndView mav = new ModelAndView();

    mav.addObject("datetime", new Date());
    mav.addObject("exception", e);
    mav.addObject("url", request.getRequestURL());
    mav.setViewName("400");
    return mav;
  }
}
