package net.xaviersala.config;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import net.xaviersala.model.FormParaula;

public class FormParaulaValidator implements Validator {

  private static final String FALTA_EL_TIPUS_DE_PARAULA = "Falta el tipus de paraula";
  private static final String NO_POT_SER_BUIT = "No pot ser buit";

  @Override
  public boolean supports(Class<?> clazz) {
    return FormParaula.class.equals(clazz);    
  }

  @Override
  public void validate(Object objecte, Errors errors) {
    
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tipus", "", FALTA_EL_TIPUS_DE_PARAULA);
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "singularMasculi", "", NO_POT_SER_BUIT);
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "singularFemeni", "", NO_POT_SER_BUIT);
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pluralMasculi", "", NO_POT_SER_BUIT);
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pluralFemeni", "", NO_POT_SER_BUIT);
    
    // FormParaula formulari = (FormParaula) objecte;
    
   
    
  }

}
