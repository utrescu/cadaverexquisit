package net.xaviersala.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.http.HttpStatus;

/**
 * La idea és personalitzar les pàgines d'errors. 
 * 
 * @author xavier
 *
 */
public class Personalitzador implements EmbeddedServletContainerCustomizer {

  @Override
  public void customize(ConfigurableEmbeddedServletContainer container) {
    
    ErrorPage error401 = new ErrorPage(HttpStatus.UNAUTHORIZED, "/401.html");
    ErrorPage error404 = new ErrorPage(HttpStatus.NOT_FOUND, "/404.html");
    ErrorPage error405 = new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED, "/405.html");
    ErrorPage error500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500.html");
    container.addErrorPages(error401, error405, error404, error500);
  }

}
