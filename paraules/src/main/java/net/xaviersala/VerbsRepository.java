package net.xaviersala;

import org.springframework.data.repository.PagingAndSortingRepository;

import net.xaviersala.model.Verb;

/**
 * Recuperar els Verbs de la base de dades.
 * 
 * @author xavier
 *
 */
public interface VerbsRepository extends PagingAndSortingRepository<Verb, Long>{

}
