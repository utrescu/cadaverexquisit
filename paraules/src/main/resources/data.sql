insert into articles (singular_masculi, singular_femeni, plural_masculi, plural_femeni, exception, exception_singular_masculi, exception_singular_femeni, exception_plural_masculi, exception_plural_femeni) values ('El', 'La', 'Els', 'Les', true, 'L''', 'L''', 'Els ', 'Les ');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('catxalot', 'balena', 'catxalots', 'balenes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('Damià', 'Martina', 'Damians', 'Martines');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('ànec', 'anegueta', 'ànecs', 'aneguetes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('raspall', 'aspiradora', 'raspall', 'aspiradores');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('animal', 'planta', 'animals', 'plantes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sabó', 'esponja', 'sabons', 'esponges');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('dofí', 'sardina', 'dofins', 'sardines');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bolet', 'flor', 'bolets', 'flors');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('lleó', 'lleona', 'lleons', 'lleones');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('gat', 'gata', 'gats', 'gates');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('got', 'tassa', 'gots', 'tasses');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('home', 'dona', 'homes', 'dones');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('barret', 'sabata', 'barrets', 'sabates');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('Joan', 'Joana', 'Joans', 'Joanes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('tren', 'bicicleta', 'trens', 'bicicletes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('taburet', 'cadira', 'taburets', 'cadires');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('roc', 'pedra', 'rocs', 'pedres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('biberó', 'bufanda', 'biberons', 'bufandes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('meló', 'cirera', 'melons', 'cireres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sofà', 'butaca', 'sofas', 'butaques');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('vi', 'cervesa', 'vins', 'cerveses');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('clot', 'porta', 'clots', 'portes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('paper', 'llibreta', 'papers', 'llibretes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sol', 'pluja', 'estels', 'gotes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('nen', 'nena', 'nens', 'nenes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cafè', 'beguda', 'cafès', 'begudes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('martell', 'escopeta', 'martells', 'escopetes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('mirall', 'finestra', 'miralls', 'finestres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('covard', 'covarda', 'covards', 'covardes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('policia', 'policia', 'policies', 'policies');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('peix', 'planta', 'peixos', 'plantes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sabó', 'menta', 'sabons', 'mentes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fluorescent', 'bombeta', 'fluorescents', 'bombetes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bolso', 'bossa', 'bolsos', 'bosses');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('retrat', 'foto', 'retrats', 'fotos');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rentaplats', 'rentadora', 'rentaplats', 'rentadores');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('llibre', 'revista', 'llibres', 'revistes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cotxe', 'moto', 'cotxes', 'motos');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('diamant', 'perla', 'diamants', 'perles');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pollastre', 'gallina', 'pollastres', 'gallines');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('femer', 'comuna', 'femers', 'comunes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('dia', 'nit', 'dies', 'nits');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('jersei', 'samarreta', 'jerseis', 'samarretes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pis', 'casa', 'pisos', 'cases');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('Frederic', 'Marta', 'Frederics', 'Martes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('vent', 'neu', 'vents', 'nevades');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('gos', 'gossa', 'gossos', 'gosses');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bistec', 'llonza', 'bistecs', 'llonces');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('toro', 'vaca', 'toros', 'vaques');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('roser', 'rosa', 'rosers', 'roses');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('televisor', 'ràdio', 'televisors', 'ràdios');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('ratolí', 'rata', 'ratolins', 'rates');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rellotge', 'torre', 'rellotges', 'torres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pa', 'galeta', 'pans', 'galetes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('aire', 'brisa', 'aires', 'brises');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('país', 'nació', 'paisos', 'nacions');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('superheroi', 'superheroïna', 'superherois', 'superheroïnes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('malparlat', 'malparlada', 'malparlats', 'malparlades');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fantasma', 'fantasma', 'fantasmes', 'fantasmes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bruixot', 'bruixa', 'bruixots', 'bruixes');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('castell', 'torre', 'castells', 'torres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('castell', 'torre', 'castells', 'torres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('treballador', 'treballadora', 'treballadors', 'treballadores');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pallaso', 'pallasa', 'pallasos', 'pallases');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('barraca', 'casa', 'barraques', 'cases');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('alcalde', 'alcaldessa', 'alcaldes', 'alcaldesses');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('soldat', 'soldada', 'soldats', 'soldades');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cavaller', 'cavallera', 'cavallers', 'cavallera');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('guerrer', 'guerrera', 'guerrers', 'guerreres');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rei', 'reina', 'reis', 'reines');
insert into noms (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('princep', 'princesa', 'princeps', 'princeses');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('menja', 'menja', 'mengen', 'mengen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('agafa', 'agafa', 'agafen', 'agafen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('puja', 'puja', 'pugen', 'pugen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('salta', 'salta', 'salten', 'salten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('tira', 'tira', 'tiren', 'tiren');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('espera', 'espera', 'esperen', 'esperen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('beu', 'beu', 'beuen', 'beuen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('aixafa', 'aixafa', 'aixafen', 'aixafen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('escalfa', 'escalfa', 'escalfen', 'escalfen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('atura', 'atura', 'aturen', 'aturen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('diu', 'diu', 'diuen', 'diuen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('trepitxa', 'trepitxa', 'trepitxen', 'trepitxen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('canta', 'canta', 'canten', 'canten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('dorm', 'dorm', 'dormen', 'dormen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('perd', 'perd', 'perden', 'perden');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('tem', 'tem', 'temen', 'temen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('perd', 'perd', 'perden', 'perden');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('és', 'és', 'són', 'són');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('té', 'té', 'tenen', 'tenen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('mata', 'mata', 'maten', 'maten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('busca', 'busca', 'busquen', 'busquen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('abraça', 'abraça', 'abracen', 'abracen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('acaricia', 'acaricia', 'acaricien', 'acaricien');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('arrossega', 'arrossega', 'arrosseguen', 'arrosseguen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('mira', 'mira', 'miren', 'miren');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('buida', 'buida', 'buiden', 'buiden');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bufa', 'bufa', 'bufen', 'bufen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cau', 'cau', 'cauen', 'cauen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('clava', 'clava', 'claven', 'claven');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('compta', 'compta', 'compten', 'compten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('doblega', 'doblega', 'dobleguen', 'dobleguen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('empeny', 'empeny', 'empenyen', 'empenyen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('encén', 'encén', 'encenen', 'encenen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('enganxa', 'enganxa', 'enganxen', 'enganxen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('enxampa', 'enxampa', 'enxampen', 'enxampen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('escolta', 'escolta', 'escolten', 'escolten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('esgarrapa', 'esgarrapa', 'esgarrapen', 'esgarrapen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('espera', 'espera', 'esperen', 'esperen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('esprem', 'esprem', 'espremen', 'espremen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('guanya', 'guanya', 'guanyen', 'guanyen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('llança', 'llança', 'llancen', 'llancen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('mossega', 'mossega', 'mosseguen', 'mosseguen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('neteja', 'neteja', 'netegen', 'netegen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('obre', 'obre', 'obren', 'obren');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('olora', 'olora', 'oloren', 'oloren');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('para', 'para', 'paren', 'paren');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pessiga', 'pessiga', 'pessiguen', 'pessiguen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pinta', 'pinta', 'pinten', 'pinten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rega', 'rega', 'reguen', 'reguen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('renya', 'renya', 'renyen', 'renyen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rescata', 'rescata', 'rescaten', 'rescaten');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('saluda', 'saluda', 'saluden', 'saluden');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('subratlla', 'subratlla', 'subratllen', 'subratllen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('talla', 'talla', 'tallen', 'tallen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('tanca', 'tanca', 'tanquen', 'tanquen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('toca', 'toca', 'toquen', 'toquen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('trepitxa', 'trepitxa', 'trepitxen', 'trepitxen');
insert into verbs (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('xiula a', 'xiula a', 'xiulen a', 'xiulen a');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bonic', 'bonica', 'bonics', 'boniques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('blau', 'blaua', 'blaus', 'blaues');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('verd', 'verda', 'verds', 'verdes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('vermell', 'vermella', 'vermells', 'vermelles');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('blanc', 'blanca', 'blancs', 'blanques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fresc', 'fresca', 'frescos', 'fresques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('alt', 'alta', 'alts', 'altes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fort', 'forta', 'forts', 'fortes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('espavilat', 'espavilada', 'espavilats', 'espavilades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('agosarat', 'agosarada', 'agosarats', 'agosarades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('adust', 'adusta', 'adustos', 'adustes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('agut', 'aguda', 'aguts', 'agudes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('alegre', 'alegre', 'alegres', 'alegres');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('antipatic', 'antipatica', 'antipatics', 'antipatiques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('apassionat', 'apassionada', 'apassionats', 'apassionades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('assenyat', 'assenyada', 'assenyats', 'assenyades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('astut', 'astuta', 'astuts', 'astutes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('atent', 'atenta', 'atents', 'atentes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('aturat', 'aturada', 'aturats', 'aturades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('aviciat', 'aviciada', 'aviciats', 'aviciades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('babau', 'babaua', 'babaus', 'babaues');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('badoc', 'badoca', 'badocs', 'badoques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('baliga-balaga', 'baliga-balaga', 'baliga-balagues', 'baliga-balagues');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('beneit', 'beneita', 'beneits', 'beneites');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('beneït', 'beneïda', 'beneïts', 'beneïdes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('brétol', 'brétola', 'brétols', 'brétoles');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('bufanúvols', 'bufanúvols', 'bufanúvols', 'bufanúvols');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('burleta', 'burleta', 'burletes', 'burletes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cagat', 'cagada', 'cagats', 'cagades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('calmós', 'calmosa', 'calmosos', 'calmoses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('caparrut', 'caparruda', 'caparruts', 'caparrudes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('caragirat', 'caragirada', 'caragirats', 'caragirades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('caustic', 'caustica', 'caustics', 'caustiques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cínic', 'cínica', 'cínics', 'cíniques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('competent', 'competent', 'competents', 'competents');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('confiat', 'confiada', 'confiats', 'confiades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('conformista', 'conformista', 'conformistes', 'conformistes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('consentit', 'consentida', 'consentits', 'consentides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('contestatari', 'contestataria', 'contestataris', 'contestataries');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('covard', 'covarda', 'covards', 'covardes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('valent', 'valenta', 'valents', 'valentes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('cregut', 'creguda', 'creguts', 'cregudes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('culte', 'culta', 'cultes', 'cultes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('decidit', 'decidida', 'dedidits', 'decidides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('desordenat', 'desordenada', 'desordenats', 'desordenades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('despert', 'desperta', 'desperts', 'despertes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('despistat', 'despistada', 'despistats', 'despistades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('divertit', 'divertida', 'divertits', 'divertides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('dócil', 'dócil', 'dócils', 'dócils');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('educat', 'educada', 'educats', 'educades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('egoista', 'egoista', 'egoistes', 'egoistes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('entremaliat', 'entremaliada', 'entremaliats', 'entremaliades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('egocéntric', 'egocéntrica', 'egocéntrics', 'egocéntriques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('egoista', 'egoista', 'egoistes', 'egoistes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('eixelebrat', 'eixelebrada', 'eixelebrats', 'eixelebrades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('encantador', 'encantadora', 'encantadors', 'encantadores');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('enginyós', 'enginyosa', 'enginyosos', 'enginyoses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('entusiasta', 'entusiasta', 'entusiastes', 'entusiastes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('espléndid', 'espléndida', 'espléndits', 'espléndides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('esquerp', 'esquerpa', 'esquerps', 'esquerpes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('exigent', 'exigent', 'exigents', 'exigents');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('extravagant', 'extravagant', 'extravagants', 'extravagants');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fals', 'falsa', 'falsos', 'falses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fanàtic', 'fanàtica', 'fanàtics', 'fanàtiques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fanfarró', 'fanfarrona', 'fanfarrons', 'fanfarrones');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fatxenda', 'fatxenda', 'fatxendes', 'fatxendes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('feble', 'feble', 'febles', 'febles');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('fidel', 'fidel', 'fidels', 'fidels');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('franc', 'franca', 'francs', 'franques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('gasiu', 'gasiva', 'gasius', 'gasives');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('genial', 'genial', 'genials', 'genials');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('generós', 'generosa', 'generosos', 'generoses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('immadur', 'immadura', 'immadurs', 'immadures');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('ingenu', 'ingenua', 'ingenus', 'ingenues');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('irresponsable', 'irresponsable', 'irresponsables', 'irresponsables');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('irónic', 'ironica', 'irónics', 'ironiques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('insolent', 'insolenta', 'insolents', 'insolentes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('llepafils', 'llepafils', 'llepafils', 'llepafils');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('llest', 'llesta', 'assenyats', 'assenyades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('malparlat', 'malparlada', 'malparlats', 'malparlades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('malpensat', 'malpensada', 'malpensats', 'malpensades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('mentider', 'mentidera', 'mentiders', 'mentideres');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('meticulós', 'meticulosa', 'meticulosos', 'meticuloses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('mesquí', 'mesquina', 'mesquins', 'mesquines');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('normal', 'normal', 'normals', 'normals');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('observador', 'observadora', 'observadors', 'observadores');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('orgullós', 'orgullosa', 'orgullosos', 'orgulloses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('panxacontent', 'panxacontenta', 'panxacontents', 'panxacontentes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pedant', 'pedant', 'pedants', 'pedantes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('perdonavides', 'perdonavides', 'perdonavides', 'perdonavides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('pinxo', 'pinxa', 'pinxos', 'pinxes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('poca-solta', 'poca-solta', 'poca-soltes', 'poca-soltes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('polit', 'polida', 'polits', 'polides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('poruc', 'poruga', 'porucs', 'porugues');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('presumit', 'presumida', 'presumits', 'presumides');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('primmirat', 'primmirada', 'primmirats', 'primmirades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('prudent', 'prudent', 'prudents', 'prudents');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rebel', 'rebel', 'rebels', 'rebels');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rialler', 'riallera', 'riallers', 'rialleres');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('rondinaire', 'rondinaire', 'rondinaires', 'rondinaires');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('seré', 'serena', 'serens', 'serenes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('seriós', 'seriosa', 'seriosos', 'serioses');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('set-ciéncies', 'set-ciéncies', 'set-ciéncies', 'set-ciéncies');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('simpatic', 'simpatica', 'simpatics', 'simpatiques');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sincer', 'sincera', 'sincers', 'sinceres');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sociable', 'sociable', 'sociables', 'sociables');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('solitari', 'solitaria', 'solitaris', 'solitaries');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('somiatruites', 'somiatruites', 'somiatruites', 'somiatruites');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('sorrut', 'sorruda', 'sorruts', 'sorrudes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('tibat', 'tibada', 'tibats', 'tibades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('tossut', 'tossuda', 'tossuts', 'tossudes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('treballador', 'treballadora', 'treballadors', 'treballadores');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('trempat', 'trempada', 'trempats', 'trempades');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('trist', 'trista', 'tristos', 'tristes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('valent', 'valenta', 'valents', 'valentes');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('viu', 'viua', 'vius', 'viues');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('xerraire', 'xerraire', 'xerraires', 'xerraires');
insert into adjectius (singular_masculi, singular_femeni, plural_masculi, plural_femeni) values ('ximplet', 'ximpleta', 'ximplets', 'ximpletes');

