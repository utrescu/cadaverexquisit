*** Keywords ***

Anar a la pàgina principal
  Log		                          Anar a l'arrel
  Go to 			                    ${web} 

Anar a la pàgina                  [Arguments]              ${adreca}
   Log                            Anar a ${adreca}
   Go to                          ${web}${adreca}  

Comprova Titol de la pàgina principal
   Title Should be 		            ${titolHome}

Mira Que Hi Hagi 5 Paraules
   ${paraules}=			              Get WebElements 			   jquery=div.paraula
   ${Count}=			                Get Length				       ${paraules}
   Should Be Equal As Integers	  ${Count}				         5
   Log		                        Hi ha ${Count} paraules 

Comprova l'enllaç de la pàgina principal
   Comprova enllaç a	            /add

Clica l'enllaç de la pàgina principal
   Clica enllaç a                 /add

Recarrega pàgina
   Log                            Recarrega
   Reload Page

Comprova els enllaços als formularis
  : FOR   ${que}      IN          @{mots}
  \    Log                        Comprovant ${que}
  \    Comprova enllaç a          /add/${que} 

Clica un dels enllaços i mira si va al formulari correcte
  ${quin}=                        Get From List  	${mots}   0
  Clica enllaç a                  /add/${quin}
  Comprova el formulari           ${quin}

Comprova el formulari             [Arguments]   ${que} 
   Page Should Contain            Afegir ${que}
   Page Should Contain Textfield  id=singularm
   Page Should Contain Textfield  id=singularf
   Page Should Contain Textfield  id=pluralm
   Page Should Contain Textfield  id=pluralf
   Page Should Contain Button     tag=button

Desa al formulari                 [arguments]    @{llista}
   Input Text                     id=singularm   @{llista}[0]
   Input Text                     id=singularf   @{llista}[1] 
   Input Text                     id=pluralm     @{llista}[2]
   Input Text                     id=pluralf     @{llista}[3] 
   Click Button                   tag=button        
   Page Should Contain            Desat (@{llista}[0], @{llista}[1], @{llista}[2], @{llista}[3])

Comprova que dóna error           [Arguments]   ${code}
   Page Should Contain            Error
   Page Should Contain            ${code}
   Comprova enllaç a              /

Comprova enllaç a                 [Arguments]   ${que}
   Page Should Contain Link	      xpath=//a[@href='${que}']

Clica enllaç a                    [Arguments]   ${que}
   Click Link                     xpath=//a[@href='${que}']

Obrir navegador
   Open browser 		              about:

Tanca navegadors
   Close all browsers
