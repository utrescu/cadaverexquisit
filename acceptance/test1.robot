*** Settings ***
Library	    			Collections
Library     			Selenium2Library
Resource          common/accionsKeywords.robot
Resource          common/variables.robot 

Test Setup  			Obrir navegador
Test Teardown			Tanca navegadors

*** Test Cases ***

Comprova pagina principal 
  [Documentation]   Comprova que la pàgina principal dóna *5 resultats* cada cop 
  Anar a la pàgina principal
  Comprova Titol de la pàgina principal
  Mira Que Hi Hagi 5 Paraules
  Comprova l'enllaç de la pàgina principal
  Recarrega pàgina
  Comprova Titol de la pàgina principal
  Mira Que Hi Hagi 5 Paraules
  Comprova l'enllaç de la pàgina principal

Comprova que apareixen els enllaços a les pàgines d'alta
  [Documentation]   Comprova que es carrega la pàgina d'ajuda 
  Anar a la pàgina principal
  Clica l'enllaç de la pàgina principal  
  Comprova els enllaços als formularis

Comprova que es pot donar d'alta una paraula 
  [Documentation]   Dóna d'alta una paraula a la base de dades
  Anar a la pàgina principal
  Clica l'enllaç de la pàgina principal  
  Clica un dels enllaços i mira si va al formulari correcte
  Desa al formulari             @{noms_exemple}

Comprova que una adreça incorrecta dóna error 404
  [Documentation]   Comprova que accedint a una adreça no mapejada dóna error
  Anar a la pàgina               /a_error
  Comprova que dóna error        404

Comprova que intentar afegir un tipus incorrecte dóna error 400
  [Documentation]   Comprova que intentar arribar a un formulari fals dóna error
  Anar a la pàgina               /add/patata
  Comprova que dóna error        400
